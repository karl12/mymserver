package uwl.pro.mymServer.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import uwl.pro.mymServer.client.MymServerService;
import uwl.pro.mymServer.client.User;
import uwl.pro.mymServer.client.VideoInfo;

import com.google.gdata.client.youtube.YouTubeService;
import com.google.gdata.data.media.MediaFileSource;
import com.google.gdata.data.media.mediarss.MediaCategory;
import com.google.gdata.data.media.mediarss.MediaDescription;
import com.google.gdata.data.media.mediarss.MediaKeywords;
import com.google.gdata.data.media.mediarss.MediaTitle;
import com.google.gdata.data.youtube.VideoEntry;
import com.google.gdata.data.youtube.VideoFeed;
import com.google.gdata.data.youtube.YouTubeMediaGroup;
import com.google.gdata.data.youtube.YouTubeNamespace;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class MymServerServiceImpl extends RemoteServiceServlet implements
		MymServerService {
	private ArrayList<User> users;

	@Override
	public User confirmLogin(User u) {
		try {
			loadUsers();

			for (int i = 0; i < users.size(); i++) {
				User user = users.get(i);
				if (u.equals(users.get(i))) {
					return user;
				}
			}
		} catch (Exception e) {
			System.out.println("Load Failed - Creating test user");

			User testUser1 = new User();
			testUser1
					.addYouTubeURL("http://youtube.googleapis.com/v/gXjHSbyJuPY");
			users = new ArrayList<User>();
			users.add(testUser1);
			saveUsers();
			return null;
		}
		return null;
	}

	public Boolean saveUsers() {
		try {
			FileOutputStream usersFile = new FileOutputStream("users.mym");
			ObjectOutputStream saveStream = new ObjectOutputStream(usersFile);
			saveStream.writeObject(users);
			saveStream.close();
		} catch (IOException e) {
			System.out.println("Save failed!");
			return false;
		}
		return true;
	}

	private void loadUsers() throws IOException, ClassNotFoundException {
		users = new ArrayList();
		FileInputStream usersFile = new FileInputStream("users.mym");
		ObjectInputStream loadStream = new ObjectInputStream(usersFile);
		users = (ArrayList<User>) loadStream.readObject();
		loadStream.close();
	}

	@Override
	public Boolean saveUser(User u) {
		for (int i = 0; i < users.size(); i++) {
			User user = users.get(i);
			if (u.equals(users.get(i))) {
				users.remove(i);
				users.add(u);
				saveUsers();
				return true;
			}
		}
		return false;
	}

	@Override
	public void uploadVideoToYoutube(User u, String fileName) {

		try {
			System.out.println(fileName);

			File f = new File(fileName);
			f.createNewFile();
			boolean exists = f.exists();

			System.out.println(exists);

			VideoEntry newEntry = new VideoEntry();

			YouTubeMediaGroup mg = newEntry.getOrCreateMediaGroup();
			mg.setTitle(new MediaTitle());
			mg.getTitle().setPlainTextContent(f.getName());
			MediaFileSource ms = new MediaFileSource(new File((f.getName())),
					"video/quicktime");
			
			mg.addCategory(new MediaCategory(YouTubeNamespace.CATEGORY_SCHEME,
					"Autos"));
			mg.setKeywords(new MediaKeywords());
			mg.getKeywords().addKeyword("test");
			mg.setDescription(new MediaDescription());
			mg.getDescription().setPlainTextContent("test");
			mg.setPrivate(false);
			mg.addCategory(new MediaCategory(
					YouTubeNamespace.DEVELOPER_TAG_SCHEME, "test"));
			newEntry.setMediaSource(ms);

			String uploadUrl = "http://uploads.gdata.youtube.com/feeds/api/users/default/uploads";

			VideoEntry createdEntry = createService(u).insert(
					new URL(uploadUrl), newEntry);

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private YouTubeService createService(User u) {
		String developerKey = "AI39si6fcwgu2g2BLIITeiH3QvwPX_C45jNmSIOUnubs50O4bXvRBf-52gnnzNRVClZ9s4EEth4FKMJOeYlTsrcW-vUk-rwGHg";
		YouTubeService service = new YouTubeService("mymServer", developerKey);

		System.out.println("Service created");

		try {
			service.setUserCredentials(u.getGoogleUserName(),
					u.getGooglePassword());
			return service;
		} catch (AuthenticationException e) {
			System.out.println("Authentication Error!");
			return null;
		}
	}

	@Override
	public ArrayList<VideoInfo> getOwnVideoFeed(User u) {
		ArrayList<VideoInfo> videoList = new ArrayList<VideoInfo>();

		String userName = u.getGoogleUserName().split("@")[0];

		// by default youtube id is different from google username, here youtube
		// id is used
		String feedUrl = "http://gdata.youtube.com/feeds/api/users/" + userName
				+ "/uploads";

		System.out.println(feedUrl);

		VideoFeed videoFeed;
		try {
			videoFeed = createService(u).getFeed(new URL(feedUrl),
					VideoFeed.class);
			for (VideoEntry ve : videoFeed.getEntries()) {
				videoList.add(videoEntryToVideoInfo(ve));
			}
		} catch (IOException | ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return videoList;
	}

	@Override
	public boolean deleteVideo(String id, User u) {
		String userName = u.getGoogleUserName().split("@")[0];
		String feedUrl = "http://gdata.youtube.com/feeds/api/users/" + userName
				+ "/uploads";

		System.out.println(feedUrl);

		VideoFeed videoFeed;
		try {
			videoFeed = createService(u).getFeed(new URL(feedUrl),
					VideoFeed.class);
			for (VideoEntry ve : videoFeed.getEntries()) {
				if (ve.getId().equalsIgnoreCase(id)){
					ve.delete();
					return true;
				}
			}
		} catch (IOException | ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	private VideoInfo videoEntryToVideoInfo(VideoEntry ve) {
		VideoInfo vi = new VideoInfo();
		vi.setId(ve.getId());
		vi.setTitle(ve.getTitle().getPlainText());
		vi.setUri(ve.getMediaGroup().getPlayer().getUrl());
		return vi;
	}
}