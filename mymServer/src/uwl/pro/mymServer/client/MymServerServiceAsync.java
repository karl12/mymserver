package uwl.pro.mymServer.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface MymServerServiceAsync {

	void confirmLogin(User u, AsyncCallback<User> callback);

	void saveUser(User u, AsyncCallback<Boolean> callback);

	void uploadVideoToYoutube(User user, String fi,
			AsyncCallback<Void> callback);

	void getOwnVideoFeed(User u, AsyncCallback<ArrayList<VideoInfo>> callback);

	void deleteVideo(String id, User u, AsyncCallback<Boolean> callback);

}
