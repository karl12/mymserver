package uwl.pro.mymServer.client;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private ArrayList<String> youTubeURLs;
	private ArrayList<String> serverVideos;
	private ArrayList<String> friendNames;

	private String googleUserName;
	private String googlePassword;

	// TODO - encrypt password

	private String password;

	public User() {
		setUsername("test");
		setPassword("test");
		setGoogleUserName("testacc2824@gmail.com");
		setGooglePassword("y3110w87");
		youTubeURLs = new ArrayList<String>();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean equals(User ld) {
		if (ld.getUsername().equals(this.getUsername())
				&& ld.getPassword().equals(this.getPassword()))
			return true;
		return false;
	}

	public ArrayList<String> getYouTubeURLs() {
		return youTubeURLs;
	}

	public void addYouTubeURL(String url) {
		youTubeURLs.add(url);
	}

	public ArrayList<String> getServerVideos() {
		return serverVideos;
	}

	public void setServerVideos(ArrayList<String> serverVideos) {
		this.serverVideos = serverVideos;
	}

	public void addServerVideo(String name) {
		serverVideos.add(name);
	}

	public ArrayList<String> getFriendNames() {
		return friendNames;
	}

	public void setFriendNames(ArrayList<String> friendNames) {
		this.friendNames = friendNames;
	}

	public void addFriendName(String name) {
		friendNames.add(name);
	}

	public String getGoogleUserName() {
		return googleUserName;
	}

	public void setGoogleUserName(String googleUserName) {
		this.googleUserName = googleUserName;
	}

	public String getGooglePassword() {
		return googlePassword;
	}

	public void setGooglePassword(String googlePassword) {
		this.googlePassword = googlePassword;
	}
}
