package uwl.pro.mymServer.client.uploader;

import uwl.pro.mymServer.client.MymServer;
import gwtupload.client.IFileInput;
import gwtupload.client.IUploadStatus.Status;
import gwtupload.client.IUploader;
import gwtupload.client.SingleUploader;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;

public class FileUploadTab extends Composite{
	public FileUploadTab() {
		HorizontalPanel panel = new HorizontalPanel();
		SingleUploader uploader = new SingleUploader();

		initWidget(panel);
		
		panel.add(uploader);
		
		uploader.addOnFinishUploadHandler(onFinishUploaderHandler);
	}

	private IUploader.OnFinishUploaderHandler onFinishUploaderHandler = new IUploader.OnFinishUploaderHandler() {

		@Override
		public void onFinish(IUploader uploader) {
			if (uploader.getStatus() == Status.SUCCESS) {
				Window.alert("Success!");
				System.out.println("File upload success! "
						+ uploader.getServerResponse());
				
				AsyncCallback<Void> callback = new AsyncCallback<Void>() {
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
					}

					@Override
					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						
					}
				};
				MymServer.getServerServ().uploadVideoToYoutube(MymServer.getUser(), uploader.getFileName(), callback);
			}
		}
	};

}
