package uwl.pro.mymServer.client.uploader;

import uwl.pro.mymServer.client.MymServer;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;

public class YoutubeLinkTab extends Composite {
	public YoutubeLinkTab() {

		final Button btnSubmit = new Button("Submit");
		final HorizontalPanel uploaderPanel = new HorizontalPanel();
		final TextBox addURL = new TextBox();

		uploaderPanel.setSize("298px", "32px");

		uploaderPanel.setVisible(false);

		addURL.setWidth("187px");

		btnSubmit.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				String input = addURL.getText();
				String embedVideoCode;
				if (input.matches(".*www.youtube.com.*") == false) {
					// TODO - add error
					return;
				}
				String videoCode = input.split(".*v=")[1];
				embedVideoCode = "http://youtube.googleapis.com/v/"
						+ videoCode;

				MymServer.getUser().addYouTubeURL(embedVideoCode);
				addURL.setText("");
				UploadMedia.updateUser();
			}
		});
		
		initWidget(uploaderPanel);

		uploaderPanel.add(btnSubmit);
		uploaderPanel.add(addURL);
	}
}
