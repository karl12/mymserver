package uwl.pro.mymServer.client.uploader;

import uwl.pro.mymServer.client.ErrorPopUp;
import uwl.pro.mymServer.client.MymServer;
import gwtupload.client.IUploadStatus.Status;
import gwtupload.client.IUploader;
import gwtupload.client.SingleUploader;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.TabPanel;

public class UploadMedia extends Composite {
	public UploadMedia() {

		TabPanel tabPanel = new TabPanel();
		YoutubeLinkTab yut = new YoutubeLinkTab();
		FileUploadTab fut = new FileUploadTab();

		tabPanel.add(yut, "Youtube");
		tabPanel.add(fut, "File Upload");
		
		initWidget(tabPanel);
	}

	public static void updateUser() {
		if (MymServer.getServerServ() == null) {
			MymServer.setServerServ();
		}

		AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {
			public void onFailure(Throwable caught) {
				ErrorPopUp epu = new ErrorPopUp("Failed user updated");
				epu.setVisible(true);
			}

			@Override
			public void onSuccess(Boolean result) {
				// TODO - something
			}
		};
		MymServer.getServerServ().saveUser(MymServer.getUser(), callback);
	}

}
