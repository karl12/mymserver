package uwl.pro.mymServer.client;

import java.util.ArrayList;

import uwl.pro.mymServer.client.uploader.UploadMedia;
import uwl.pro.mymServer.client.videobrowser.MediaBrowser;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class MymServer implements EntryPoint {

	private static MediaBrowser mB;
	private static Loginner loginScreen;
	private static RootPanel rootPanel;
	private static VerticalPanel bodyVP;
	protected static boolean loggedIn;
	protected static ArrayList<String> videoDB;
	private static MymServerServiceAsync serverServ = GWT
			.create(MymServerService.class);
	private static User user;
	
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	/**
	 * Create a remote service proxy to talk to the server-side Greeting
	 * service.
	 */
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	private MenuItem mntmAdd;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		
		videoDB = new ArrayList<String>();
		loggedIn = false;

		final Label errorLabel = new Label();
		final UploadMedia uM = new UploadMedia();
		
		loginScreen = new Loginner();
		rootPanel = RootPanel.get("body");
		rootPanel.setPixelSize(600, 600);
		
		mB = new MediaBrowser();

		bodyVP = new VerticalPanel();
		rootPanel.add(bodyVP, 63, 100);
		bodyVP.setSize("437px", "233px");

		setBody(loginScreen);
		

		MenuBar browseMenuBar = new MenuBar(false);
		rootPanel.add(browseMenuBar, 10, 10);

		MenuItem mntmBrowse = new MenuItem("Browse", false, new Command() {
			public void execute() {
				if (loggedIn == true) {
					setBody(mB);
				}
			}
		});
		browseMenuBar.addItem(mntmBrowse);

		MenuBar addMenuBar = new MenuBar(false);
		rootPanel.add(addMenuBar, 497, 10);

		mntmAdd = new MenuItem("Add", false, new Command() {
			public void execute() {
				if (loggedIn == true) {
					setBody(uM);
				}
			}
		});
		addMenuBar.addItem(mntmAdd);

	}

	protected static void logIn(User u) {
		loggedIn = true;
		user = u;
		setBody(mB);
	}

	private static void setBody(Widget w) {
		bodyVP.clear();
		bodyVP.add(w);
	}
	
	public static User getUser(){
		return user;
	}

	public static MymServerServiceAsync getServerServ() {
		return serverServ;
	}

	public static void setServerServ() {
		MymServer.serverServ = GWT.create(MymServerService.class);
	}
}