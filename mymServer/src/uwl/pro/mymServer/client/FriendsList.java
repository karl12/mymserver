package uwl.pro.mymServer.client;

import java.util.ArrayList;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;

public class FriendsList extends FlexTable{

	public FriendsList(){
		ArrayList<String> usersFriends = MymServer.getUser().getFriendNames();
		
		for (int i = 0; i < usersFriends.size(); i++){
			setText(i, 0, usersFriends.get(i));
			
			Button chatBtn = new Button(">");
			chatBtn.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					// TODO - connect to chat box
					
				}
			});
		}
	}
}
