package uwl.pro.mymServer.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("mymServerLogins")
public interface MymServerService extends RemoteService {

	User confirmLogin(User u);
	Boolean saveUser(User u);
	void uploadVideoToYoutube(User user, String fi);
	ArrayList<VideoInfo> getOwnVideoFeed(User u);
	boolean deleteVideo(String id, User u);
}