package uwl.pro.mymServer.client;

import java.io.Serializable;

public class VideoInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8282701412121116946L;
	String title;
	String url;
	String id;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUri() {
		return url;
	}
	public void setUri(String uri) {
		this.url = uri;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
