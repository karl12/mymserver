package uwl.pro.mymServer.client;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Button;

public class ChatBox extends Composite{
	public ChatBox(String chatPartner) {
		
		VerticalPanel verticalPanel = new VerticalPanel();
		initWidget(verticalPanel);
		
		TextArea textArea = new TextArea();
		verticalPanel.add(textArea);
		textArea.setSize("100%", "80%");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel.add(horizontalPanel);
		horizontalPanel.setSize("100%", "30%");
		
		TextBox textBox = new TextBox();
		horizontalPanel.add(textBox);
		textBox.setWidth("80%");
		
		Button btnSend = new Button("Send");
		horizontalPanel.add(btnSend);
		
		
	}	
}
