package uwl.pro.mymServer.client.videobrowser;

import java.util.ArrayList;

import uwl.pro.mymServer.client.ErrorPopUp;
import uwl.pro.mymServer.client.MymServer;
import uwl.pro.mymServer.client.User;
import uwl.pro.mymServer.client.VideoInfo;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;

public class OwnVideoTab extends com.google.gwt.user.client.ui.Composite {

	private ArrayList<VideoInfo> videoList;
	private HTML youtubeHTMLVideo;
	private FlexTable videoURLTable;

	public OwnVideoTab() {
		final Label errorLabel = new Label();
		videoList = new ArrayList<VideoInfo>();

		videoURLTable = new FlexTable();
		youtubeHTMLVideo = new HTML("YouTube Video", true);

		AbsolutePanel aPanel = new AbsolutePanel();
		aPanel.setPixelSize(600, 800);
		HorizontalPanel videoPanel = new HorizontalPanel();

		videoPanel.setSize("232px", "170px");

		videoPanel.add(videoURLTable);
		videoURLTable.setText(0, 0, "URL");

		videoPanel.add(youtubeHTMLVideo);
		youtubeHTMLVideo.setSize("201px", "88px");
		youtubeHTMLVideo.setVisible(false);

		aPanel.add(videoURLTable);
		aPanel.add(videoPanel);

		initWidget(aPanel);

		refresh();
	}

	public void refresh() {

		AsyncCallback<ArrayList<VideoInfo>> callback = new AsyncCallback<ArrayList<VideoInfo>>() {
			public void onFailure(Throwable caught) {
				ErrorPopUp epu = new ErrorPopUp("Video Lookup Failed!");
				epu.setVisible(true);
			}

			@Override
			public void onSuccess(ArrayList<VideoInfo> result) {
				if (result != null)
					videoList = result;
				else {
					ErrorPopUp epu = new ErrorPopUp("No Videos Found");
					epu.setVisible(true);
				}
			}
		};
		MymServer.getServerServ()
				.getOwnVideoFeed(MymServer.getUser(), callback);

		if (videoList.isEmpty() == true)
			return;

		for (int i = 0; i < videoList.size(); i++) {
			final VideoInfo vi = videoList.get(i);

			videoURLTable.setText(i, 0, vi.getTitle());

			// Add player button
			Button playerVideoBtn = new Button(">");
			playerVideoBtn.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					youtubeHTMLVideo.setHTML("<embed width=\"420\" height=\"345\" "
							+ "src=\"" + vi.getUri() + "\""
							+ "type=\"application/x-shockwave-flash\">"
							+ "</embed>");

					youtubeHTMLVideo.setVisible(true);
				}
			});
			videoURLTable.setWidget(i, 2, playerVideoBtn);

			// add delete button
			Button deleteVideoBtn = new Button("X");
			deleteVideoBtn.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {
						public void onFailure(Throwable caught) {
							ErrorPopUp epu = new ErrorPopUp(
									"Failed to delete video.");
							epu.setVisible(true);
						}

						@Override
						public void onSuccess(Boolean result) {
							// TODO :- add confirmation
						}

					};
					MymServer.getServerServ().deleteVideo(vi.getId(),
							MymServer.getUser(), callback);
					refresh();
				}
			});
			videoURLTable.setWidget(i, 1, deleteVideoBtn);
		}

	}
}
