package uwl.pro.mymServer.client.videobrowser;

import uwl.pro.mymServer.client.uploader.FileUploadTab;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TabPanel;

public class MediaBrowser extends Composite {

	public MediaBrowser() {
		TabPanel tabPanel = new TabPanel();
		final LinkedVideosTab lvTab = new LinkedVideosTab();
		final OwnVideoTab ovTab = new OwnVideoTab();
		
		tabPanel.add(ovTab, "My Videos");
		tabPanel.add(lvTab, "Linked Videos");
		
		tabPanel.addSelectionHandler(new SelectionHandler<Integer>() {
			
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				
				lvTab.refresh();
				ovTab.refresh();
			}
		});
		
		initWidget(tabPanel);
		
	}
	
	

}
