package uwl.pro.mymServer.client.videobrowser;

import java.util.ArrayList;

import uwl.pro.mymServer.client.MymServer;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;

public class LinkedVideosTab extends Composite {
	private HTML youtubeHTML;
	private FlexTable videoURLTable;

	public LinkedVideosTab() {
		final Label errorLabel = new Label();
		
		videoURLTable = new FlexTable();
		youtubeHTML = new HTML("YouTube Video", true);

		AbsolutePanel aPanel = new AbsolutePanel();
		aPanel.setPixelSize(600, 800);
		HorizontalPanel videoPanel = new HorizontalPanel();

		videoPanel.setSize("232px", "170px");

		videoPanel.add(videoURLTable);
		videoURLTable.setText(0, 0, "URL");

		videoPanel.add(youtubeHTML);
		youtubeHTML.setSize("201px", "88px");
		youtubeHTML.setVisible(false);

		aPanel.add(videoPanel);
		
		initWidget(aPanel);
	}

	public void refresh() {
		videoURLTable.clear();
		ArrayList<String> userURLList = MymServer.getUser().getYouTubeURLs();

		if (userURLList.isEmpty() == true)
			return;
		
		for (int i = 0; i < userURLList.size(); i++) {
			final String html = userURLList.get(i);
			
			videoURLTable.setText(i, 0, html);

			// Add player button
			Button playerVideoBtn = new Button(">");
			playerVideoBtn.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					youtubeHTML.setHTML("<embed width=\"420\" height=\"345\" "
							+ "src=\"" + html + "\""
							+ "type=\"application/x-shockwave-flash\">"
							+ "</embed>");

					youtubeHTML.setVisible(true);
				}
			});
			videoURLTable.setWidget(i, 3, playerVideoBtn);
		}

	}
}
