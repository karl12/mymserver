package uwl.pro.mymServer.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Loginner extends Composite {
	public Loginner() {

		VerticalPanel verticalPanel = new VerticalPanel();
		initWidget(verticalPanel);
		verticalPanel.setSize("184px", "159px");

		Label lblUsername = new Label("Username");
		verticalPanel.add(lblUsername);

		final TextBox usernameTB = new TextBox();
		verticalPanel.add(usernameTB);

		Label lblPassword = new Label("Password");
		verticalPanel.add(lblPassword);

		final PasswordTextBox passTB = new PasswordTextBox();
		verticalPanel.add(passTB);

		Button btnSubmit = new Button("Submit");
		btnSubmit.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				final User loginDetails = new User();
				loginDetails.setUsername(usernameTB.getText());
				loginDetails.setPassword(passTB.getText());

				if (MymServer.getServerServ() == null) {
					MymServer.setServerServ();
				}

				AsyncCallback<User> callback = new AsyncCallback<User>() {
					public void onFailure(Throwable caught) {
						ErrorPopUp epu = new ErrorPopUp("Failed user lookup.");
						epu.setVisible(true);
					}

					@Override
					public void onSuccess(User result) {
						if (result != null)
							MymServer.logIn(result);
						else {
							ErrorPopUp epu = new ErrorPopUp(
									"User not found!");
							epu.setVisible(true);
						}
					}
				};
				MymServer.getServerServ().confirmLogin(loginDetails, callback);
			}
		});
		verticalPanel.add(btnSubmit);
	}
}
