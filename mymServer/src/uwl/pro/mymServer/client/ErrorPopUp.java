package uwl.pro.mymServer.client;

import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;

public class ErrorPopUp extends Composite {
	public ErrorPopUp(String message) {
		
		VerticalPanel verticalPanel = new VerticalPanel();
		initWidget(verticalPanel);
		verticalPanel.setSize("347px", "20px");
		
		Label lblError = new Label(("Error: " + message));
		verticalPanel.add(lblError);
		
		Button btnConfirm = new Button("Confirm");
		btnConfirm.addClickHandler(new ClickHandler() {
			@UiHandler("btnConfirm")
			public void onClick(ClickEvent event) {
				removeFromParent();
			}
		});
		verticalPanel.add(btnConfirm);
	}

}
